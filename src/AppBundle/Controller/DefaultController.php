<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Application\Sonata\MediaBundle\Entity\Media;
use AppBundle\Form\FeedbackType;
use AppBundle\Entity\Feedback;
use AppBundle\Entity\Product;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Value;
use AppBundle\Entity\SecondValue;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/catalog", name="catalog")
     * @Route("/catalog/{slug}", name="category")
     * @Route("/catalog/page/{page}", name="catalog_page")
     * @Route("/catalog/{slug}/page/{page}", name="category_page")
     */
    public function categoryAction(Request $request, $slug = null, $page = null)
    {
      $locale = $request->getLocale();
      $locale = ($locale == 'en') ? '' : $locale;
      $functions = array('title' => 'getTitle', 'sd' => 'getShortDescription');
      if ($locale != '') foreach ($functions as &$function) $function .= ($locale == 'uk') ? 'Ua' : 'Ru';

      $page = $page ?: 1;
      $cat_id = null;
      $page_products = 6;
      $page_offset = $page_products*($page-1);

      if ($slug) {
        $categories = $this->getDoctrine()->getRepository('ApplicationSonataClassificationBundle:Category')->findOneBySlug($slug);
        $cat_id = $categories->getId();
      }

      $params = $cat_id ? array('category' => $cat_id) : array();
      $product_count = count($this->getDoctrine()->getRepository('AppBundle:Product')->findBy($params));
      $pages_total = ceil($product_count / $page_products);
      $products = $this->getDoctrine()->getRepository('AppBundle:Product')->findBy($params, null, $page_products, $page_offset);
      $dump = array();
      foreach ($products as $product) {
        $dump[] = array(
          'id' => $product->getId(),
          'sku' => $product->getSku(),
          'title' => $product->{$functions['title']}(),
          'media' => $product->getImage() ?: $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media')->find('1'),
          'text' => $product->{$functions['sd']}(),
        );
      }

      $categories = $this->getDoctrine()->getRepository('ApplicationSonataClassificationBundle:Category')->findAll();
      foreach ($categories as $key => $cat) {
        if ($key != 0) {
          $category[] = array(
            'name' => $cat->getName(),
            'slug' => $cat->getSlug(),
          );
        }
      }

      return $this->render('default/catalog.html.twig', array(
        'dump' => $dump,
        'category' => $category,
        'slug' => $slug,
        'page' => $page,
        'pages_total' => $pages_total
      ));
    }

    /**
     * @Route("/product/{id}", name="product")
     */
    public function productAction(Request $request, $id)
    {
      $locale = $request->getLocale();
      $locale = ($locale == 'en') ? '' : $locale;
      $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
      $product = $repository->findOneBySku($id);
      $options = $product->getAttributes();
      $option = array();
      $functions = array('name' => 'getName', 'title' => 'getTitle', 'sd' => 'getShortDescription', 'body' => 'getBody');
      if ($locale != '') foreach ($functions as &$function) $function .= ($locale == 'uk') ? 'Ua' : 'Ru';

      if (count($options) > 0) {
        $option[0] = array('id' => $options->first()->getId(), 'name' => $options->first()->{$functions['name']}());
        $option[0]['values'] = array();
        foreach ($options->first()->getValues() as $value) {
          $option[0]['values'][] = array('id' => $value->getId(), 'name' => $value->getName());
        }
      }
      if (count($options) == 2) $option[1] = array('id' => $options->last()->getId(), 'name' => $options->last()->{$functions['name']}());

      $dump = array(
        'sku' => $product->getSku(),
        'title' => $product->{$functions['title']}(),
        'description' => $product->{$functions['sd']}(),
        'media' => $product->getImage() ?: $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media')->find('1'),
        'text' => $product->{$functions['body']}(),
      );

      $em = $this->getDoctrine()->getEntityManager();
      for ($i=5; $i > 2; $i--) {
        $res = $em->createQuery('select a from AppBundle\Entity\Product a where a.sku like :sku')
                  ->setParameter('sku', substr($product->getSku(), 0, $i).'%')->getResult();
        if (count($res) > 3) break;
      }

      $related_products = array();
      $j = 0;
      foreach ($res as $related_product) {
        if ($related_product->getSku() === $product->getSku()) continue;
        if (count($related_products) === 3) break;
        $related_products[] = array(
          'sku' => $related_product->getSku(),
          'title' => $related_product->{$functions['title']}(),
          'media' => $related_product->getImage() ?: $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media')->find('1'),
        );
      }

      return $this->render('default/product.html.twig', array(
        'dump' => $dump,
        'option' => $option,
        'related' => $related_products,
      ));
    }

    /**
     * @Route("/contacts", name="contacts")
     */
    public function contactsAction(Request $request)
    {
        $prodSku = $request->request->get('sku');
        $prodSku = $prodSku ? $prodSku : '';
        $feedback = new Feedback();
        $feedback->setProductSku($prodSku);
        $form = $this->createForm(new FeedbackType(), $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedback = $form->getData();
            $feedback->setDate(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($feedback);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/contacts.html.twig', array(
        'form' => $form->createView(),
        'sku' => $prodSku,
      ));
    }

    /**
     * @Route("/api/get-values", name="values")
     */
    public function ValuesAction(Request $request)
    {
      $value = $request->request->get('value');

      $values = $this->getDoctrine()->getRepository('AppBundle:Value')->find($value)->getSecondValues();
      $return = array();
      foreach ($values as $value) {
        $return[] = array('id' => $value->getId(), 'name' => $value->getName());
      }

      return new Response(json_encode($return));
    }

    /**
     * @Route("/api/get-sku", name="sku")
     */
    public function SkuAction(Request $request)
    {
      $value = $request->request->get('value');
      $selectType = $request->request->get('selectType');

      $rep = ($selectType == 1) ? '' : 'Second';
      $sku = $this->getDoctrine()->getRepository('AppBundle:'.$rep.'Value')->find($value)->getSku();

      return new Response($sku);
    }

    /**
     * @Route("/db", name="db")
     */
    public function DbAction()
    {
      $arr = array('surgery' => 2, 'prosthetics' => 3, '3d-navigation' => 4);
      foreach ($arr as $akey => $avalue) {
      $em = $this->getDoctrine()->getManager();
      $category = $this->getDoctrine()->getRepository('ApplicationSonataClassificationBundle:Category')->find($avalue);
      $products = $this->getDoctrine()->getRepository('AppBundle:Product');
      $file = file_get_contents('/home/kowalski0805/'.$akey.'_upd.txt');
      $json = json_decode($file, true);

      foreach ($json as $j) {
        if (!$products->findOneBySku($j['sku'])) {
          $product = new Product();
          $product
            ->setTitle($j['name'])
            ->setSku($j['sku'])
            ->setShortDescription($j['short-description'])
            ->setBody($j['description'])
            ->setCategory($category);
          if (array_key_exists('option_names', $j)) {
            if (count($j['option_names']) == 1) {
              $attribute = new Attribute();
              $attribute->setName($j['option_names'][0])->setType(1);
              $values = $j['option_values'];
              foreach ($values as $value) {
                $val = new Value();
                $val->setName($value[0]);
                $val->setSku($value[1]);
                $attribute->addValue($val);
                $val->setAttr($attribute);
                $em->persist($val);
              }
              $product->addAttribute($attribute);
              $attribute->setProd($product);
              $em->persist($attribute);
            } else {
              $attribute = new Attribute();
              $attribute->setName($j['option_names'][0])->setType(1);
              $values = $j['option_values'];
              foreach ($values as $key => $value) {
                $val = new Value();
                $val->setName($key);
                foreach ($value as $secValue) {
                  $secVal = new SecondValue();
                  $secVal->setName($secValue[0]);
                  $secVal->setSku($secValue[1]);
                  $val->addSecondValue($secVal);
                  $secVal->setValue($val);
                  $em->persist($secVal);
                }
                $attribute->addValue($val);
                $val->setAttr($attribute);
                $em->persist($val);
              }
              $attribute->setProd($product);
              $em->persist($attribute);
              $attribute = new Attribute();
              $attribute->setName($j['option_names'][1])->setType(2);
              $product->addAttribute($attribute);
              $attribute->setProd($product);
              $em->persist($attribute);
            }
          }
          $em->persist($product);
          $em->flush();
        }
      }
    }
      return new Response('Write success!');
    }

    /**
     * @Route("/update", name="update")
     */
    public function UpdateAction()
    {
      $arr = array('surgery' => 2, 'prosthetics' => 3, '3d-navigation' => 4);
      foreach ($arr as $akey => $avalue) {
        $em = $this->getDoctrine()->getManager();
        $rep = $this->getDoctrine()->getRepository('AppBundle:Product');
        $file = file_get_contents('/home/kowalski0805/'.$akey.'_slocale.txt');
        $json = json_decode($file, true);

        foreach ($json as $j) {
          $product = $rep->findOneBySku($j['sku']);
          if ($product == null) {
            die($j['sku']);
          }
          $product->setTitleUa($j['name_ua'])
                  ->setTitleRu($j['name_ru'])
                  ->setShortDescriptionUa($j['short-description_ua'])
                  ->setShortDescriptionRu($j['short-description_ru'])
                  ->setBodyUa($j['description_ua'])
                  ->setBodyRu($j['description_ru']);
          $attrs = $product->getAttributes();
          $c = count($j['option_names_ua']);
          if ($c == 1) {
            $af = $attrs->first();
            $af->setNameUa($j['option_names_ua'][0])->setNameRu($j['option_names_ru'][0]);
            $em->persist($af);
          }
          if ($c == 2) {
            $af = $attrs->first();
            $af->setNameUa($j['option_names_ua'][0])->setNameRu($j['option_names_ru'][0]);
            $al = $attrs->last();
            $al->setNameUa($j['option_names_ua'][1])->setNameRu($j['option_names_ru'][1]);
            $em->persist($af);
            $em->persist($al);
          }
          $em->persist($product);
          $em->flush();
        }
      }
      return new Response('Update success!');
    }

    /**
     * @Route("/image", name="image")
     */
    public function ImageAction()
    {
      $arr = array('surgery' => 2, 'prosthetics' => 3, '3d-navigation' => 4);
      foreach ($arr as $akey => $avalue) {
        $em = $this->getDoctrine()->getManager();
        $rep = $this->getDoctrine()->getRepository('AppBundle:Product');
        $file = file_get_contents('/home/kowalski0805/'.$akey.'.txt');
        $json = json_decode($file, true);
        $mediaManager = $this->container->get('sonata.media.manager.media');

        foreach ($json as $j) {
          if ((substr($j['image'], -18) != 'img/no-picture.jpg') && ($j['image'] != '')) {
            $product = $rep->findOneBySku($j['sku']);
            if (!$product->getImage()) {
              $uploadImage = file_get_contents($j['image']);
              file_put_contents(substr($j['image'], -18), $uploadImage);
              $media = new Media();
              $media->setBinaryContent(substr($j['image'], -18));
              $media->setContext('default');
              $media->setProviderName('sonata.media.provider.image');
              $mediaManager->save($media);
              $product->setImage($media);
              $em->persist($product);
            }
          }
        }
        $em->flush();
    }
    return new Response('Image success!');
  }

  /**
   * @Route("/getimage", name="getimage")
   */
  public function GetImageAction()
  {
    $json = array();
    $em = $this->getDoctrine()->getManager();
    $rep = $this->getDoctrine()->getRepository('AppBundle:Product')->findAll();
    foreach ($rep as $product) {
      $title = $product->getTitle();
      $img = $product->getImage();
      if ($img) {
        $ref = $img->getProviderReference();
        $json[] = array('title' => $title, 'img' => $ref);
      }
    }
    file_put_contents('/home/kowalski0805/img.json', json_encode($json, JSON_PRETTY_PRINT));
    return new Response('Success!');

  }

  /**
   * @Route("/setlocale/{loc}", name="setlocale")
   */
  public function SetLocaleAction(Request $request, $loc)
  {
    $referer = $request->headers->get('referer');
    $url = explode('/', $referer);
    unset($url[0], $url[1], $url[2]);
    $url[3] = $loc;
    $link = implode('/', $url);
    return new RedirectResponse('/'.$link);
  }
}
