<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="feedback")
 */
class Feedback
{
    /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\Column(type="text", length=100)
   */
  protected $email;

  /**
   * @ORM\Column(type="string", length=100)
   */
  protected $name;

  /**
   * @ORM\Column(type="text")
   */
  protected $message;

  /**
   * @ORM\Column(type="string", length=100)
   */
  protected $productSku;

  /**
   * @ORM\Column(type="datetime")
   */
  protected $date;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Set productSku
     *
     * @param string $productSku
     * @return Feedback
     */
    public function setProductSku($productSku)
    {
        $this->productSku = $productSku;
    
        return $this;
    }

    /**
     * Get productSku
     *
     * @return string 
     */
    public function getProductSku()
    {
        return $this->productSku;
    }
}