<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Attribute
 *
 * @ORM\Table(name="attribute")
 * @ORM\Entity
 */
class Attribute
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="attributes")
     */
    private $prod;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ua", type="string", length=255, nullable=true)
     */
    private $nameUa;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ru", type="string", length=255, nullable=true)
     */
    private $nameRu;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Value", mappedBy="attr", cascade={"remove"})
     */
    private $values;

    public function __construct() {
        $this->values = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prodId
     *
     * @param integer $prodId
     * @return Attribute
     */
    public function setProdId($prodId)
    {
        $this->prodId = $prodId;

        return $this;
    }

    /**
     * Get prodId
     *
     * @return integer
     */
    public function getProdId()
    {
        return $this->prodId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Attribute
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Attribute
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add values
     *
     * @param \AppBundle\Entity\Value $values
     * @return Attribute
     */
    public function addValue(\AppBundle\Entity\Value $values)
    {
        $this->values[] = $values;

        return $this;
    }

    /**
     * Remove values
     *
     * @param \AppBundle\Entity\Value $values
     */
    public function removeValue(\AppBundle\Entity\Value $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set prod
     *
     * @param \AppBundle\Entity\Product $prod
     * @return Attribute
     */
    public function setProd(\AppBundle\Entity\Product $prod = null)
    {
        $this->prod = $prod;

        return $this;
    }

    /**
     * Get prod
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProd()
    {
        return $this->prod;
    }

    /**
     * Set nameUa
     *
     * @param string $nameUa
     * @return Attribute
     */
    public function setNameUa($nameUa)
    {
        $this->nameUa = $nameUa;

        return $this;
    }

    /**
     * Get nameUa
     *
     * @return string
     */
    public function getNameUa()
    {
        return $this->nameUa;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     * @return Attribute
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    /**
     * Get nameRu
     *
     * @return string
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }
}
