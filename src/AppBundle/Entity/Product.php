<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\ClassificationBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product.
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ua", type="string", length=255, nullable=true)
     */
    private $titleUa;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"remove"}, orphanRemoval=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string")
     */
    private $sku;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\ClassificationBundle\Entity\Category")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text")
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description_ua", type="text", nullable=true)
     */
    private $shortDescriptionUa;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description_ru", type="text", nullable=true)
     */
    private $shortDescriptionRu;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="body_ua", type="text", nullable=true)
     */
    private $bodyUa;

    /**
     * @var string
     *
     * @ORM\Column(name="body_ru", type="text", nullable=true)
     */
    private $bodyRu;

    /**
     * @ORM\OneToMany(targetEntity="Attribute", mappedBy="prod", cascade={"remove"})
     */
    private $attributes;

    public function __construct() {
        $this->attributes = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sku.
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku.
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set category.
     *
     * @param Category $category
     *
     * @return Product
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category.
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set short description.
     *
     * @param string $shortDescription
     *
     * @return Product
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get short description.
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Product
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set image.
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     * @return Product
     */
    public function addAttribute(\AppBundle\Entity\Attribute $attributes)
    {
        $this->attributes[] = $attributes;

        return $this;
    }

    /**
     * Remove attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     */
    public function removeAttribute(\AppBundle\Entity\Attribute $attributes)
    {
        $this->attributes->removeElement($attributes);
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set titleUa
     *
     * @param string $titleUa
     * @return Product
     */
    public function setTitleUa($titleUa)
    {
        $this->titleUa = $titleUa;

        return $this;
    }

    /**
     * Get titleUa
     *
     * @return string
     */
    public function getTitleUa()
    {
        return $this->titleUa;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     * @return Product
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set shortDescriptionUa
     *
     * @param string $shortDescriptionUa
     * @return Product
     */
    public function setShortDescriptionUa($shortDescriptionUa)
    {
        $this->shortDescriptionUa = $shortDescriptionUa;

        return $this;
    }

    /**
     * Get shortDescriptionUa
     *
     * @return string
     */
    public function getShortDescriptionUa()
    {
        return $this->shortDescriptionUa;
    }

    /**
     * Set shortDescriptionRu
     *
     * @param string $shortDescriptionRu
     * @return Product
     */
    public function setShortDescriptionRu($shortDescriptionRu)
    {
        $this->shortDescriptionRu = $shortDescriptionRu;

        return $this;
    }

    /**
     * Get shortDescriptionRu
     *
     * @return string
     */
    public function getShortDescriptionRu()
    {
        return $this->shortDescriptionRu;
    }

    /**
     * Set bodyUa
     *
     * @param string $bodyUa
     * @return Product
     */
    public function setBodyUa($bodyUa)
    {
        $this->bodyUa = $bodyUa;

        return $this;
    }

    /**
     * Get bodyUa
     *
     * @return string
     */
    public function getBodyUa()
    {
        return $this->bodyUa;
    }

    /**
     * Set bodyRu
     *
     * @param string $bodyRu
     * @return Product
     */
    public function setBodyRu($bodyRu)
    {
        $this->bodyRu = $bodyRu;

        return $this;
    }

    /**
     * Get bodyRu
     *
     * @return string
     */
    public function getBodyRu()
    {
        return $this->bodyRu;
    }
}
