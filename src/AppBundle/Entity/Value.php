<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Value
 *
 * @ORM\Table(name="value")
 * @ORM\Entity
 */
class Value
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="values")
     */
    private $attr;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="SecondValue", mappedBy="value", cascade={"remove"})
     */
    private $secondValues;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255, nullable=true)
     */
    private $sku;

    public function __construct() {
        $this->secondValues = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attrId
     *
     * @param integer $attrId
     * @return Value
     */
    public function setAttrId($attrId)
    {
        $this->attrId = $attrId;

        return $this;
    }

    /**
     * Get attrId
     *
     * @return integer
     */
    public function getAttrId()
    {
        return $this->attrId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Value
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add secondValues
     *
     * @param \AppBundle\Entity\SecondValue $secondValues
     * @return Value
     */
    public function addSecondValue(\AppBundle\Entity\SecondValue $secondValues)
    {
        $this->secondValues[] = $secondValues;

        return $this;
    }

    /**
     * Remove secondValues
     *
     * @param \AppBundle\Entity\SecondValue $secondValues
     */
    public function removeSecondValue(\AppBundle\Entity\SecondValue $secondValues)
    {
        $this->secondValues->removeElement($secondValues);
    }

    /**
     * Get secondValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSecondValues()
    {
        return $this->secondValues;
    }

    /**
     * Set attr
     *
     * @param \AppBundle\Entity\Attribute $attr
     * @return Value
     */
    public function setAttr(\AppBundle\Entity\Attribute $attr = null)
    {
        $this->attr = $attr;

        return $this;
    }

    /**
     * Get attr
     *
     * @return \AppBundle\Entity\Attribute
     */
    public function getAttr()
    {
        return $this->attr;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return Value
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }
}