<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\Product;

class ProductAdmin extends AbstractAdmin
{
    protected $translationDomain = 'ProductAdmin';

    public function toString($object)
    {
        return $object instanceof Product
            ? $object->getTitle()
            : 'entity.product'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => false);
        if ($image && ($webPath = $image->getImage()->getName())) {
            $fileFieldOptions['help'] = '<p>'.$webPath.'</p>';
        }

        $formMapper
            ->tab('tab.general')
                ->with('column.metadata', array('class' => 'col-md-6'))
                    ->add('sku', 'text', array(
                      'label' => 'form.sku'
                    ))
                    ->add('category', 'sonata_type_model', array(
                      'class' => 'Application\Sonata\ClassificationBundle\Entity\Category',
                      'property' => 'name',
                      'label' => 'form.category'
                    ))
                ->end()
                ->with('column.image', array('class' => 'col-md-6'))
                    ->add('image', 'sonata_type_model_list', $fileFieldOptions, array(
                      'link_parameters' => array(
                        'context' => 'default',
                        'provider' => 'sonata.media.provider.image',
                      ),
                    ))
                ->end()
            ->end()
            ->tab('tab.english')
                    ->add('title', 'text', array(
                      'label' => 'form.title'
                    ))
                    ->add('shortDescription', 'textarea', array(
                      'label' => 'form.short_description'
                    ))
                    ->add('body', 'textarea', array(
                      'attr' => array('rows' => '5'),
                      'label' => 'form.body'
                    ))
                ->end()
            ->end()
            ->tab('tab.russian')
                    ->add('titleRu', 'text', array(
                      'label' => 'form.title'
                    ))
                    ->add('shortDescriptionRu', 'textarea', array(
                      'label' => 'form.short_description'
                    ))
                    ->add('bodyRu', 'textarea', array(
                      'attr' => array('rows' => '5'),
                      'label' => 'form.body'
                    ))
                ->end()
            ->end()
            ->tab('tab.ukrainian')
                    ->add('titleUa', 'text', array(
                      'label' => 'form.title'
                    ))
                    ->add('shortDescriptionUa', 'textarea', array(
                      'label' => 'form.short_description'
                    ))
                    ->add('bodyUa', 'textarea', array(
                      'attr' => array('rows' => '5'),
                      'label' => 'form.body'
                    ))
                ->end()
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image', 'image', array(
                'header_style' => 'width: 120px;',
                'row_align' => 'center',
                'label' => 'column.image'
            ))
            ->addIdentifier('sku', null, array(
                'header_style' => 'width: 5%;',
                'row_align' => 'center',
                'label' => 'form.sku'
            ))
            ->addIdentifier('titleUa', null, array(
                'label' => 'form.title'
            ));
    }
}
