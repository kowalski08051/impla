<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null,  array('label' => 'contacts.form.name'))
            ->add('email', 'email', array('label' => 'contacts.form.email'))
            ->add('message', 'textarea',  array('label' => 'contacts.form.message'))
            ->add('productSku', 'hidden')
            ->add('save', 'submit', array('label' => 'contacts.form.submit'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'AppBundle\Entity\Feedback'
        ));
    }

    public function getName()
    {
        return 'app_feedback';
    }
}
